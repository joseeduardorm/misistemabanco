package Operaciones;

import Dto.Cuenta;
import java.time.LocalDate;

public class Operacion {

    private Cuenta cuenta;
    private double saldo;
    private int identificador;
    private LocalDate fecha;

    public Operacion() {
    }

    public boolean retirarAhorro(double valor) {
        return false;
    }

    public boolean retirarCorriente(double valor) {
        return false;

    }

    public boolean Transferir(double valor, Cuenta cuenta) {
        return false;

    }

    public boolean consignar(double valor) {
        return false;
    }

    public Cuenta getCuenta() {
        return cuenta;
    }

    public void setCuenta(Cuenta cuenta) {
        this.cuenta = cuenta;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public int getIdentificador() {
        return identificador;
    }

    public void setIdentificador(int identificador) {
        this.identificador = identificador;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

}
