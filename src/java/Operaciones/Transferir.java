/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Operaciones;

import Dto.Cuenta;

/**
 *
 * @author edye
 */
public class Transferir extends Operacion{
    
    
    int numeroCuenta;
    
    @Override
    public boolean Transferir (double valor, Cuenta cuenta){
        
        if(this.getCuenta().getClass().getSimpleName()=="CuentaAhorro"){
             if(super.retirarAhorro(valor)&cuenta.consignar(valor)){
             return true;
         }
        }
        
        else{
        
             if(super.retirarCorriente(valor)&cuenta.consignar(valor)){
             return true;
         }
        }
       
            return false;
       
    }
    
    
}
