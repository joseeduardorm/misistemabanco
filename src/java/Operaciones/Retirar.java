/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Operaciones;

/**
 *
 * @author edye
 */
public class Retirar  extends Operacion{
    
    
    @Override
    public boolean retirarAhorro (double valor){
        if (this.getSaldo()-valor >= 0){
    this.setSaldo(this.getSaldo() - valor); 
    return true;
    }
        return false;
    }
    
    @Override
    public boolean retirarCorriente (double valor){
        if (this.getSaldo() - valor >= -1000000){
    this.setSaldo(this.getSaldo() - valor); 
    return true;
    }
        return false;
    }
      
}
