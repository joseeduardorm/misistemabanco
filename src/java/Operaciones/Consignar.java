package Operaciones;

public class Consignar extends Operacion {

    public Consignar() {
        super();
    }
    
    

    @Override
    public boolean consignar(double valor) {

        if (valor > 0) {
            this.setSaldo(this.getSaldo() + valor);
            return true;
        }
        return false;
    }

}
