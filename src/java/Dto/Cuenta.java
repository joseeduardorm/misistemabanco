
package Dto;

import Operaciones.Operacion;
import java.time.LocalDate;

public class Cuenta implements Comparable{
    
    private long nroCuenta;
    private Cliente cliente;
    private double saldo;
    private LocalDate fechaCreacion;
    private Operacion operacion;
    public Cuenta() {
    }

    public long getNroCuenta() {
            return nroCuenta;
    }

    public void setNroCuenta(long nroCuenta) {
        this.nroCuenta = nroCuenta;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public LocalDate getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(LocalDate fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Override
    public int compareTo(Object o) {
      Cuenta cuenta=(Cuenta)o;
      return (int)(this.nroCuenta-cuenta.nroCuenta);
    }

    @Override
    public String toString() {
        return "Cuenta{" + "nroCuenta=" + nroCuenta + "tipo cuenta=" + nombre() + ", cliente=" + cliente.getCedula() + ", saldo=" + saldo + ", fechaCreacion=" + fechaCreacion + '}';
    }
    
     public String nombre() {
    return this.getClass().getSimpleName();
   }
     
     public boolean retirarAhorro (double valor){
        return this.operacion.retirarAhorro(valor);
     }
    
      public boolean retirarCorriente (double valor){
        return this.operacion.retirarCorriente(valor);
      
    }
    
      public boolean Transferir (double valor,Cuenta cuenta){
        return this.operacion.Transferir(valor, cuenta);
       
    }
     
    public boolean consignar (double valor){
        return this.operacion.consignar(valor);
    } 
    
    
}
