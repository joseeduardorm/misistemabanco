<%-- 
    Document   : registroexistoso
    Created on : 17/10/2019, 09:19:07 AM
    Author     : docente
--%>

<%@page import="Dto.Cliente"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Negocio.Banco"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="./css/estilo.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <title>Registro exitoso</title>
    </head>
    <body>
             
 <!––  linea de banner--> 
            <header  id="banner" >
                    <div class="row"  >
                            
                        <div class="col-lg-9">
                          <h1>'</h1>
                            <h1 class="text-center" >BANCO MIS AHORROS</h1>
                        </div>
                    </div>
            </header>
   
        <%
        Banco banco=(Banco)(request.getSession().getAttribute("banco"));
        request.getSession().setAttribute("banco", banco);
            
        %>
        
        <h1 class="register-title">Registro exitoso</h1>
        <br>
        <hr>
        <% for (Cliente dato:banco.getClientes())
        {
        %>
            <p><%=dato.toString()%></p>
        <%
            }
        %>
        
        <hr>
        
         <div class="row"  >
               <div class="col-lg-2"></div>
             <div class="col-lg-4">
                <p>
                    <a type="button" href="./jsp/Cliente/registrar.jsp">Ingresar otro cliente</a>
                </p>
             </div>
               
               <div class="col-lg-4">
                <p>
                    <a type="button" href="./index.html">Volver a inicio</a>
                </p>
             </div>
           </div>
        
             <footer id ="defecto">
	<section class="main row">
 		<article class="col-lg-12">
	
		 <div >
	    	<p class ="center"  >
	    		Copyright © AÑO 2019
	        	Autor: jose eduardo rozo molina
	        | 	Cod: 1151619
	        |	Correo: joseesuardorm@ufps.edu.co 
	    	 </p>
	    </article>

	    <article class="col-lg-12">
	    	<p class ="text-center">
	    		Desarrollo De Aplicaciones Web -
	    	
	        <a href="http://ingsistemas.ufps.edu.co/" target="_blank">Ing.Sistemas</a> - 
	        <a target="_blank" href="http://ufps.edu.co/">UFPS</a>
	        
	    	</p>
	        
	   	 </div>
		</article>
	</section>
</footer>
    </body>
</html>
