
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="./css/estilo.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <title>Error</title>
    </head>
    <body>

        <!––  linea de banner--> 
        <header  id="banner" >
            <div class="row"  >

                <div class="col-lg-9">
                    <h1>'</h1>
                    <h1 class="text-center" >BANCO MIS AHORROS</h1>
                </div>
            </div>
        </header>

        <%
            String error = (String) (request.getSession().getAttribute("error"));
        %>
        <h1 class="register-title">Hubo un error: <%=error%></h1>
        <p>
            <a href="index.html">Inicio</a>
        </p>
        <p>
            <a href="jsp/Cuenta/registroCta.jsp">Regresar</a>
        </p>
        
        <footer id ="defecto">
            <section class="main row">
                <article class="col-lg-12">

                    <div >
                        <p class ="center"  >
                            Copyright © AÑO 2019
                            Autor: jose eduardo rozo molina
                            | 	Cod: 1151619
                            |	Correo: joseesuardorm@ufps.edu.co 
                        </p>
                </article>

                <article class="col-lg-12">
                    <p class ="text-center">
                        Desarrollo De Aplicaciones Web -

                        <a href="http://ingsistemas.ufps.edu.co/" target="_blank">Ing.Sistemas</a> - 
                        <a target="_blank" href="http://ufps.edu.co/">UFPS</a>

                    </p>

                    </div>
                </article>
            </section>
        </footer>
    </body>
</html>
